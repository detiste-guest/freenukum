// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: Wolfgang Silbermayr <wolfgang@silbermayr.at>

use crate::{
    actor::{
        ActParameters, Actor, ActorExt, CreateActorWithDetails,
        RenderParameters,
    },
    level::tiles::LevelTiles,
    Hero, HorizontalDirection, RangedIterator, Result, Sizes,
    OBJECT_HOSTILESHOT,
};
use sdl2::rect::{Point, Rect};

#[derive(Debug)]
pub(crate) struct HostileShot {
    tile: usize,
    frame: RangedIterator,
    position: Rect,
    is_alive: bool,
    direction: HorizontalDirection,
}

impl CreateActorWithDetails for HostileShot {
    type Details = HorizontalDirection;

    fn create_with_details(
        direction: HorizontalDirection,
        pos: Point,
        sizes: &dyn Sizes,
        _tiles: &mut LevelTiles,
    ) -> Actor {
        let tile = match direction {
            HorizontalDirection::Left => OBJECT_HOSTILESHOT,
            HorizontalDirection::Right => OBJECT_HOSTILESHOT + 2,
        };

        Actor::HostileShot(Self {
            tile,
            frame: RangedIterator::new(2),
            position: Rect::new(
                pos.x,
                pos.y,
                sizes.width(),
                sizes.height(),
            ),
            is_alive: true,
            direction,
        })
    }
}

impl ActorExt for HostileShot {
    fn act(&mut self, p: ActParameters) {
        let offset =
            (p.sizes.width() as i32) * self.direction.as_factor_i32();
        self.position.offset(offset, 0);

        self.is_alive = p
            .tiles
            .get(
                self.position.x() / p.sizes.width() as i32,
                self.position.y() / p.sizes.height() as i32,
            )
            .map(|t| !t.solid)
            .unwrap_or(false);

        self.frame.next();
    }

    fn render(&mut self, p: RenderParameters) -> Result<()> {
        p.renderer.place_tile(
            self.tile + self.frame.current(),
            self.position.top_left(),
        )?;
        Ok(())
    }

    fn position(&self) -> Rect {
        self.position
    }

    fn is_in_foreground(&self) -> bool {
        true
    }

    fn hurts_hero(&self, hero: &Hero) -> bool {
        self.position.has_intersection(hero.position.geometry)
    }

    fn is_alive(&self) -> bool {
        self.is_alive
    }

    fn acts_while_invisible(&self) -> bool {
        true
    }
}
